TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    attribute.cpp
CONFIG += c++11

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    attribute.h \
    aufgabe.h

