#ifndef AUFGABE_H
#define AUFGABE_H


//STL
#include <type_traits>
#include <cstdint>
#include <bitset>
#include <iostream>

#include "attribute.h"
class QDate{};

// Global Model Property Trait Struct
//***************************************************************************************************************
template< typename _ModelClass >
struct trait{
	using _propertyHint_t = typename _ModelClass::e_PropertyHint;

	template< _propertyHint_t value >
	struct PropertyTrait{
		using value_type = auto;
	};

	template< _propertyHint_t _hint_, typename _value_t >
	struct _propertyTrait {
		static constexpr _propertyHint_t hint = _hint_;
		using value_type = _value_t;
	};

	template< _propertyHint_t _hint >
	using attribute_value_t = typename PropertyTrait< _hint >::value_type;

	template< _propertyHint_t _hint >
	using wrapped_attribute_t = c_ObservedAttribute< attribute_value_t< _hint > >;

	template< _propertyHint_t _hint >
	using attribute_t = typename wrapped_attribute_t< _hint >::optionalValue;
};

// Aufgabe
//****************************************************************************************************************

// Enum deklaration
struct c_Aufgabe_base{
	enum struct e_PropertyHint : int {
		Person,
		Vorgang,
		Kostenstelle,
		Bemerkung,
		Datum,
		Beginn,
		Ende,
		Pause,
		Fahrtzeit,
		Null,
		NoProperty
	};

};

// Property Datum
using aufgabenPropertyTrait = trait<c_Aufgabe_base>; //Explicit deklaration
static constexpr c_Aufgabe_base::e_PropertyHint datumProperty = c_Aufgabe_base::e_PropertyHint::Datum;
using datum_t = trait<c_Aufgabe_base>::attribute_t< c_Aufgabe_base::e_PropertyHint::Datum >;

// Class Deklaration
class c_Aufgabe : public c_Aufgabe_base
{
public:
/*
	template< > struct PropertyTrait< c_Aufgabe::e_PropertyHint::Kostenstelle > : _propertyTrait< c_Aufgabe, e_PropertyHint::Kostenstelle, c_SharedKostenstelle >{	};
	template< > struct PropertyTrait< c_Aufgabe::e_PropertyHint::Vorgang > : _propertyTrait<c_Aufgabe, e_PropertyHint::Vorgang, c_SharedVorgang >{	};
	template< > struct PropertyTrait< c_Aufgabe::e_PropertyHint::Person > : _propertyTrait<c_Aufgabe, e_PropertyHint::Person, c_SharedPerson >{	};
	template< > struct PropertyTrait< c_Aufgabe::e_PropertyHint::Bemerkung > : _propertyTrait<c_Aufgabe, e_PropertyHint::Bemerkung, QString >{	};
	template< > struct PropertyTrait< c_Aufgabe::e_PropertyHint::Fahrtzeit > : _propertyTrait<c_Aufgabe, e_PropertyHint::Fahrtzeit, QTime >{	};
	template< > struct PropertyTrait< c_Aufgabe::e_PropertyHint::Beginn > : _propertyTrait<c_Aufgabe, e_PropertyHint::Beginn, QTime >{	};
	template< > struct PropertyTrait< c_Aufgabe::e_PropertyHint::Pause > : _propertyTrait<c_Aufgabe, e_PropertyHint::Pause, QTime >{	};
	template< > struct PropertyTrait< c_Aufgabe::e_PropertyHint::Ende > : _propertyTrait<c_Aufgabe, e_PropertyHint::Ende, QTime >{	};
	*/
/*
	using beginn_t = attribute_t< e_PropertyHint::Beginn>;
	using ende_t = attribute_t< e_PropertyHint::Ende>;
	using pause_t = attribute_t< e_PropertyHint::Pause >;
	using fahrtzeit_t = attribute_t< e_PropertyHint::Fahrtzeit >;
	using bemerkung_t = attribute_t< e_PropertyHint::Bemerkung >;
	using person_t = attribute_t< e_PropertyHint::Person >;
	using vorgang_t = attribute_t< e_PropertyHint::Vorgang >;
	using kostenstelle_t = attribute_t< e_PropertyHint::Kostenstelle>;
*/
public:
	c_Aufgabe();
	c_Aufgabe( const c_Aufgabe& cp ) = default;
	c_Aufgabe& operator=( const c_Aufgabe& cp ) = default;
	virtual ~c_Aufgabe( ) = default;

	template< e_PropertyHint _PHint >
	c_ObservedAttribute< aufgabenPropertyTrait::attribute_t< _PHint > >& getAttribute( );

	datum_t getDatum() const					{ return pm_Datum->getValue();	}
	void setDatum( datum_t value )				{ pm_Datum->setValue( value ); }

	/*
	kostenstelle_t getKostenstelle()	const	{ 	return pm_Kostenstelle.getValue();	}
	person_t getMitarbeiter() const			{ 	return pm_Person.getValue();	}
	vorgang_t getVorgang() const				{ 	return pm_Vorgang.getValue();	}
	bemerkung_t getBemerkung() const			{ 	return pm_Bemerkung.getValue();	}
	fahrtzeit_t getFahrtzeit() const			{ 	return pm_Fahrtzeit.getValue();	}
	beginn_t getBeginn() const					{ 	return pm_Beginn.getValue();	}
	pause_t getPause() const					{ 	return pm_Pause.getValue();	}
	ende_t getEnde() const						{ 	return pm_Ende.getValue();	}

	void setKostenstelle( c_SharedKostenstelle value ) { pm_Kostenstelle.setValue( value ); }
	void setMitarbeiter( c_SharedMitarbeiter value )	{ pm_Person.setValue( value ); }
	void setVorgang( c_SharedVorgang value )				{ pm_Vorgang.setValue( value ); }
	void setBemerkung( QString value )						{ pm_Bemerkung.setValue( value ); }
	void setFahrtzeit( QTime value )							{ pm_Fahrtzeit.setValue( value ); }
	void setBeginn( QTime value )								{ pm_Beginn.setValue( value ); }
	void setPause( QTime value )								{ pm_Pause.setValue( value ); }
	void setEnde( QTime value )								{ pm_Ende.setValue( value ); }
*/

private:
	datum_t pm_Datum;

	/*
	kostenstelle_t pm_Kostenstelle;
	person_t pm_Person;
	vorgang_t pm_Vorgang;
	bemerkung_t pm_Bemerkung;
	fahrtzeit_t pm_Fahrtzeit;
	beginn_t pm_Beginn;
	pause_t pm_Pause;
	ende_t pm_Ende;
*/
};
/*
template< > struct c_Aufgabe::PropertyTrait< c_Aufgabe::e_PropertyHint::Kostenstelle > : c_Aufgabe::_propertyTrait< c_Aufgabe::e_PropertyHint::Kostenstelle, c_SharedKostenstelle >{	};
template< > struct c_Aufgabe::PropertyTrait< c_Aufgabe::e_PropertyHint::Vorgang > : c_Aufgabe::_propertyTrait< c_Aufgabe::e_PropertyHint::Vorgang, c_SharedVorgang >{	};
template< > struct c_Aufgabe::PropertyTrait< c_Aufgabe::e_PropertyHint::Person > : c_Aufgabe::_propertyTrait< c_Aufgabe::e_PropertyHint::Person, c_SharedPerson >{	};
template< > struct c_Aufgabe::PropertyTrait< c_Aufgabe::e_PropertyHint::Bemerkung > : c_Aufgabe::_propertyTrait< c_Aufgabe::e_PropertyHint::Bemerkung, QString >{	};
template< > struct c_Aufgabe::PropertyTrait< c_Aufgabe::e_PropertyHint::Fahrtzeit > : c_Aufgabe::_propertyTrait< c_Aufgabe::e_PropertyHint::Fahrtzeit, QTime >{	};
template< > struct c_Aufgabe::PropertyTrait< c_Aufgabe::e_PropertyHint::Beginn > : c_Aufgabe::_propertyTrait< c_Aufgabe::e_PropertyHint::Beginn, QTime >{	};
template< > struct c_Aufgabe::PropertyTrait< c_Aufgabe::e_PropertyHint::Pause > : c_Aufgabe::_propertyTrait< c_Aufgabe::e_PropertyHint::Pause, QTime >{	};
template< > struct c_Aufgabe::PropertyTrait< c_Aufgabe::e_PropertyHint::Ende > : c_Aufgabe::_propertyTrait< c_Aufgabe::e_PropertyHint::Ende, QTime >{	};
template< > struct c_Aufgabe::PropertyTrait< c_Aufgabe::e_PropertyHint::Datum > : c_Aufgabe::_propertyTrait< c_Aufgabe::e_PropertyHint::Datum, QDate >{	};
*/

#endif // AUFGABE_H
