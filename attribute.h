#ifndef ATTRIBUTE_H
#define ATTRIBUTE_H
#include <memory>

template< typename _T >
class c_ObservedAttribute
{
public:
	using valueType = _T;
	using optionalValue = std::shared_ptr< _T >;

	void setValue(const optionalValue &value){
		this->pm_Value = value;

	}

	optionalValue getValue() const
	{
		return pm_Value;
	}

private:
	optionalValue pm_Value;
};
#endif // ATTRIBUTE_H
